Hi,

Thank you for emailing to the **Community Hubs Network Support**! We'll try to respond
to the emails as quickly as possible and we're tracking your request as ticket %{ISSUE_ID}.

In meanwhile, you can reply to this email thread if you have more details to your issue.

Have a nice day!

- The Pins Team

---

**Have an GitLab.com account?** Please reply to this thread mentioning your GitLab.com username in form
of `@username` in the body.
